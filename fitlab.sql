-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 19, 2018 at 04:24 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `healthdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bodypartcode`
--

CREATE TABLE IF NOT EXISTS `bodypartcode` (
  `bodycode` varchar(5) NOT NULL,
  `bodypart` varchar(50) NOT NULL,
  PRIMARY KEY  (`bodycode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bodypartcode`
--

INSERT INTO `bodypartcode` (`bodycode`, `bodypart`) VALUES
('bv001', 'Neck-(back)'),
('bv002', 'Mid-(back)'),
('bv003', 'Below Mid Back-(back)'),
('bv004', 'Lower Back-(back)'),
('bv006', 'Triceps-(back)'),
('bv007', 'Elbow-(back)'),
('bv008', 'Fore Arm-(back)'),
('bv009', 'Wrist-(back)'),
('bv010', 'Glute-(back)'),
('bv011', 'Hamstring-(back)'),
('bv012', 'Calf-(back)'),
('bv013', 'Achilles-(back)'),
('sv001', 'Neck-(side)'),
('sv002', 'Shoulder-(side)'),
('sv003', 'Triceps-(side)'),
('sv004', 'Elbow-(side)'),
('sv005', 'Fore Arm-(side)'),
('sv006', 'Wrist-(side)'),
('sv007', 'Hamstring-(side)'),
('sv008', 'Quad-(side)'),
('sv009', 'Knee-(side)'),
('sv010', 'Calf-(side)'),
('sv011', 'Ankle-(side)'),
('fv001', 'Chest-(front)'),
('fv003', 'Bicep-(front)'),
('fv004', 'Fore Arm-(front)'),
('fv005', 'Wrist-(front)'),
('fv006', 'ABS-(front)'),
('fv007', 'Quad -(front)'),
('fv008', 'Knee-(front)'),
('fv009', 'Shin-(front)'),
('fv010', 'Ankle-(front)');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category` varchar(50) NOT NULL,
  `category_id` int(2) NOT NULL,
  PRIMARY KEY  (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category`, `category_id`) VALUES
('Roller', 1),
('Stretching', 2),
('Excercise', 3);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `video_id` int(11) NOT NULL,
  `user_comment` varchar(500) NOT NULL,
  `adm_comment` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--


-- --------------------------------------------------------

--
-- Table structure for table `favourate`
--

CREATE TABLE IF NOT EXISTS `favourate` (
  `id` int(100) NOT NULL,
  `file_name` varchar(100) default NULL,
  `created_date_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `access_date_time` timestamp NOT NULL default '0000-00-00 00:00:00',
  `user_id` varchar(50) default NULL,
  `deleted_flag` int(1) default '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourate`
--

INSERT INTO `favourate` (`id`, `file_name`, `created_date_time`, `access_date_time`, `user_id`, `deleted_flag`) VALUES
(2, NULL, '2017-11-01 00:42:09', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(2, NULL, '2017-11-01 00:42:09', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(2, NULL, '2017-11-01 00:42:09', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(2, NULL, '2017-11-01 00:42:09', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-01 00:42:09', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-03 06:57:40', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-03 07:25:04', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-03 08:48:17', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(2, NULL, '2017-11-03 08:48:23', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(2, NULL, '2017-11-03 09:59:01', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-04 08:29:34', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(1, NULL, '2017-11-04 08:29:35', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(1, NULL, '2017-11-04 08:29:42', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(2, NULL, '2017-11-04 08:29:49', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(1, NULL, '2017-11-06 22:57:12', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-06 22:57:14', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-06 22:57:16', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-06 22:57:19', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-06 22:57:21', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-06 22:57:26', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-11-06 22:57:28', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-12-11 15:45:57', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-12-11 15:45:57', '0000-00-00 00:00:00', 'aaa@email.com', 1),
(1, NULL, '2017-12-14 19:33:32', '0000-00-00 00:00:00', '', 1),
(162, NULL, '2017-12-18 05:00:56', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(162, NULL, '2017-12-18 05:00:57', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(162, NULL, '2017-12-18 05:00:58', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(162, NULL, '2017-12-18 05:01:00', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(162, NULL, '2017-12-18 05:01:01', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(162, NULL, '2017-12-18 05:01:03', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(162, NULL, '2017-12-18 05:01:04', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(197, NULL, '2017-12-19 07:00:03', '0000-00-00 00:00:00', '9911900738@gmail.com', 1),
(161, NULL, '2017-12-19 08:49:13', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(161, NULL, '2017-12-19 08:49:15', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(161, NULL, '2017-12-19 08:49:16', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(161, NULL, '2017-12-19 08:49:17', '0000-00-00 00:00:00', 'ashe@betaar3.com', 1),
(144, NULL, '2017-12-26 00:39:39', '0000-00-00 00:00:00', 'ashe11@betaar3.com', 1),
(107, NULL, '2018-01-16 19:27:29', '0000-00-00 00:00:00', 'aaa@email.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id_user` int(4) NOT NULL auto_increment,
  `name_user` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL default '',
  `password_user` varchar(100) NOT NULL,
  `account_status_user` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_user`, `name_user`, `email_user`, `password_user`, `account_status_user`) VALUES
(1, 'aa', 'aaa@email.com', '111111', 1),
(22, 'aaa', 'aaa@email.com', '111111', 1),
(21, 'aaa', '9@aqq.com', '111111', 1),
(19, 'aaa', '7@aqq.com', '111111', 1),
(20, 'aaa', '8@aqq.com', '111111', 1),
(18, 'aaa', '6@aqq.com', '111111', 1),
(17, 'aaa', '4@qq.com', '111111', 0),
(16, 'ashe', 'ashe@betaar3.com', 'ashe11@@', 0),
(23, 'arvinda', 'arvinda@betaar3.com', 'arvinda1982', 1),
(24, 'mike', 'mike@betaar3.com', 'mike123', 1),
(25, 'null', 'null', 'null', 0),
(26, 'bbb', 'bbb@email.com', '111111', 0),
(27, 'bbb', 'aasdfasdfasdfasdfasdfsadf@email.com', '111111', 0),
(28, 'sdfgasdf', 'fitlab@email.com', '111111', 0),
(29, 'sdfgasdf', 'aasdfasdfasdfasdfasdfsa@email.com', '111111', 0),
(30, 'sdfgasdf', 'aaaaaaaaaaaaaaa@email.com', '111111', 0),
(31, 'sdfgasdf', 'sssss@email.com', '111111', 0),
(32, 'sdfgasdf', 'ddddd@email.com', '111111', 0),
(33, 'sdfgasdf', 'wwww@email.com', '111111', 0),
(34, 'sdfgasdf', 'qqqqqqqq@email.com', '111111', 0),
(35, 'sdfgasdf', 'bbbbrrr@email.com', '111111', 0),
(36, 'sdfgasdf', 'qqqaaa@email.com', '111111', 0),
(37, 'sdfgasdf', 'qqww@email.com', '111111', 0),
(38, 'sdfgasdf', '111aa@email.com', '111111', 0),
(39, 'sdfgasdf', 'sdsdaq@email.com', '111111', 0),
(40, 'sdfgasdf', 'aaaaaaaaaaaaaa@email.com', '111111', 0),
(41, 'sdfgasdf', 'ashe@yahoo.com', '111111', 0),
(42, 'sdfgasdf', 'qwawe123@email.com', '111111', 0),
(43, 'sdfgasdf', 'xcdes@email.com', '111111', 0),
(44, 'sdfgasdf', '0986@email.com', '111111', 0),
(45, 'sdfgasdf', 'z@email.com', '111111', 0),
(46, 'sdfgasdf', 'zz@email.com', '111111', 0),
(47, 'sdserweflffgsf', 'zzaass@yahoo.com', '111111', 0),
(48, 'sdserweflffgsf', 'zzaasswer@yahoo.com', '111111', 0),
(49, '12121212', 'z111z@email.com', '111111', 0),
(50, '12121212', 'z1111z@email.com', '111111', 0),
(51, 'xcvzxcvzxcv', 'zxcvzxcv@email.com', '111111', 0),
(52, 'bbbbb', 'bbbbb@email.com', '111111', 0),
(53, 'ashe', 'wow@betaar3.com', '121212', 0),
(54, 'arvinda', '9911900738@gmail.com', 'arvinda', 0),
(55, 'Michael Sanchez', 'docsanchezz@yahoo.com', '4125mohr', 0),
(56, 'arvinda', '991190073@gmail.com', '111111', 0),
(57, 'ar', 'arvinda11@gmail.com', '111111', 0),
(58, 'ashe', 'ashe11@betaar3.com', '222222', 0),
(59, 'ashe', 'ashe111@gmail.com', '222222', 0),
(60, 'ashe', 'ashe1111@gmail.com', '222222', 0),
(61, 'ashe', 'ashe11@gmail.com', '111111', 0);

-- --------------------------------------------------------

--
-- Table structure for table `planid`
--

CREATE TABLE IF NOT EXISTS `planid` (
  `plan_id` int(10) NOT NULL auto_increment,
  `date` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY  (`plan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `planid`
--

INSERT INTO `planid` (`plan_id`, `date`, `email`) VALUES
(1, '2017-12-10', 'aaa@email.com'),
(2, '2017-12-11', 'aaa@email.com'),
(3, '2017-12-14', 'aaa@email.com'),
(4, '2017-12-13', 'aaa@email.com'),
(5, '2017-12-12', 'aaa@email.com'),
(6, '2017-12-15', 'aaa@email.com'),
(7, '2017-12-16', 'aaa@email.com'),
(8, '2017-12-23', 'bbb@email.com'),
(9, '2017-12-21', 'bbb@email.com'),
(10, '2017-12-17', 'bbb@email.com'),
(11, '2017-12-22', 'bbb@email.com'),
(12, '2017-12-19', 'bbb@email.com'),
(13, '2017-12-18', 'bbb@email.com'),
(14, '2017-12-20', 'bbb@email.com'),
(15, '2017-12-19', 'wow@betaar3.com'),
(16, '2017-12-21', 'wow@betaar3.com'),
(17, '2017-12-23', 'wow@betaar3.com'),
(18, '2017-12-22', 'wow@betaar3.com'),
(19, '2017-12-17', 'wow@betaar3.com'),
(20, '2017-12-20', 'wow@betaar3.com'),
(21, '2017-12-30', '9911900738@gmail.com'),
(22, '2017-12-28', '9911900738@gmail.com'),
(23, '2017-12-26', '9911900738@gmail.com'),
(24, '2017-12-27', '9911900738@gmail.com'),
(25, '2017-12-29', '9911900738@gmail.com'),
(26, '2017-12-24', '9911900738@gmail.com'),
(27, '2017-12-25', '9911900738@gmail.com'),
(28, '2017-11-27', 'aaa@email.com'),
(29, '2017-11-29', 'aaa@email.com'),
(30, '2017-12-18', 'wow@betaar3.com'),
(31, '2018-01-29', 'docsanchezz@yahoo.com'),
(32, '2018-01-32', 'docsanchezz@yahoo.com'),
(33, '2018-01-28', 'docsanchezz@yahoo.com'),
(34, '2018-01-30', 'docsanchezz@yahoo.com'),
(35, '2018-01-27', 'docsanchezz@yahoo.com'),
(36, '2018-01-31', 'docsanchezz@yahoo.com'),
(37, '2018-01-26', 'docsanchezz@yahoo.com'),
(38, '2017-12-30', 'ashe@betaar3.com'),
(39, '2017-12-28', 'ashe@betaar3.com'),
(40, '2017-12-24', 'ashe@betaar3.com'),
(41, '2017-12-25', 'ashe@betaar3.com'),
(42, '2017-12-29', 'ashe@betaar3.com'),
(43, '2017-12-27', 'ashe@betaar3.com'),
(44, '2017-12-26', 'ashe@betaar3.com'),
(45, '2018-01-03', 'ashe@betaar3.com'),
(46, '2017-12-32', 'ashe@betaar3.com'),
(47, '2018-01-02', 'ashe@betaar3.com'),
(48, '2017-12-31', 'ashe@betaar3.com'),
(49, '2018-01-05', 'ashe@betaar3.com'),
(50, '2018-01-04', 'ashe@betaar3.com'),
(51, '2018-01-06', 'ashe@betaar3.com'),
(52, '2018-01-02', 'aaa@email.com'),
(53, '2017-12-32', 'aaa@email.com'),
(54, '2017-12-31', 'aaa@email.com'),
(55, '2018-01-03', 'aaa@email.com'),
(56, '2018-01-05', 'aaa@email.com'),
(57, '2018-01-04', 'aaa@email.com'),
(58, '2018-01-06', 'aaa@email.com'),
(59, '2017-12-20', 'aaa@email.com'),
(60, '2017-12-21', 'aaa@email.com'),
(61, '2017-12-19', 'aaa@email.com'),
(62, '2017-12-22', 'aaa@email.com'),
(63, '2017-12-23', 'aaa@email.com'),
(64, '2017-12-24', 'aaa@email.com'),
(65, '2017-12-25', 'aaa@email.com'),
(66, '2018-01-03', 'ashe11@betaar3.com'),
(67, '2017-12-32', 'ashe11@betaar3.com'),
(68, '2018-01-05', 'ashe11@betaar3.com'),
(69, '2018-01-04', 'ashe11@betaar3.com'),
(70, '2018-01-02', 'ashe11@betaar3.com'),
(71, '2018-01-06', 'ashe11@betaar3.com'),
(72, '2017-12-31', 'ashe11@betaar3.com'),
(73, '2017-12-29', 'ashe11@betaar3.com'),
(74, '2017-12-30', 'ashe11@betaar3.com'),
(75, '2018-01-13', 'ashe11@betaar3.com'),
(76, '2018-01-08', 'ashe11@betaar3.com'),
(77, '2018-01-12', 'ashe11@betaar3.com'),
(78, '2018-01-09', 'ashe11@betaar3.com'),
(79, '2018-01-07', 'ashe11@betaar3.com'),
(80, '2018-01-10', 'ashe11@betaar3.com'),
(81, '2018-01-11', 'ashe11@betaar3.com'),
(82, '2018-01-20', 'ashe11@betaar3.com'),
(83, '2018-01-23', 'ashe11@betaar3.com'),
(84, '2018-01-21', 'ashe11@betaar3.com'),
(85, '2018-01-24', 'ashe11@betaar3.com'),
(86, '2018-01-22', 'ashe11@betaar3.com'),
(87, '2018-01-26', 'ashe11@betaar3.com'),
(88, '2018-01-25', 'ashe11@betaar3.com');

-- --------------------------------------------------------

--
-- Table structure for table `setgoal`
--

CREATE TABLE IF NOT EXISTS `setgoal` (
  `plan_id` int(10) NOT NULL,
  `date_from` varchar(10) NOT NULL,
  `date_to` varchar(10) default NULL,
  `exercise_time` varchar(2) NOT NULL,
  `mobility_time` varchar(2) NOT NULL,
  `stretching_time` varchar(2) NOT NULL,
  `user_weight` varchar(6) NOT NULL default '0',
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setgoal`
--

INSERT INTO `setgoal` (`plan_id`, `date_from`, `date_to`, `exercise_time`, `mobility_time`, `stretching_time`, `user_weight`, `email`) VALUES
(1, '2017-12-10', NULL, '21', '21', '12', '', 'aaa@email.com'),
(2, '2017-12-11', NULL, '21', '21', '12', '', 'aaa@email.com'),
(3, '2017-12-14', NULL, '12', '12', '21', '', 'aaa@email.com'),
(4, '2017-12-13', NULL, '21', '12', '21', '', 'aaa@email.com'),
(5, '2017-12-12', NULL, '21', '21', '21', '', 'aaa@email.com'),
(6, '2017-12-15', NULL, '12', '21', '21', '', 'aaa@email.com'),
(7, '2017-12-16', NULL, '21', '21', '21', '', 'aaa@email.com'),
(8, '2017-12-23', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(9, '2017-12-21', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(10, '2017-12-17', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(11, '2017-12-22', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(12, '2017-12-19', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(13, '2017-12-18', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(14, '2017-12-20', NULL, '0', '0', '0', 'null', 'bbb@email.com'),
(15, '2017-12-19', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(16, '2017-12-21', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(17, '2017-12-23', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(18, '2017-12-22', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(19, '2017-12-17', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(20, '2017-12-20', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(21, '2017-12-30', NULL, '5', '5', '5', 'null', '9911900738@gmail.com'),
(22, '2017-12-28', NULL, '5', '5', '5', 'null', '9911900738@gmail.com'),
(23, '2017-12-26', NULL, '20', '5', '5', 'null', '9911900738@gmail.com'),
(24, '2017-12-27', NULL, '10', '5', '5', 'null', '9911900738@gmail.com'),
(25, '2017-12-29', NULL, '5', '5', '5', 'null', '9911900738@gmail.com'),
(26, '2017-12-24', NULL, '10', '10', '10', 'null', '9911900738@gmail.com'),
(27, '2017-12-25', NULL, '20', '5', '5', 'null', '9911900738@gmail.com'),
(28, '2017-11-27', NULL, '10', '10', '10', 'null', 'aaa@email.com'),
(30, '2017-12-18', NULL, '0', '0', '0', 'null', 'wow@betaar3.com'),
(31, '2018-01-29', NULL, '7', '7', '0', 'null', 'docsanchezz@yahoo.com'),
(32, '2018-01-32', NULL, '0', '0', '0', 'null', 'docsanchezz@yahoo.com'),
(33, '2018-01-28', NULL, '5', '5', '1', 'null', 'docsanchezz@yahoo.com'),
(34, '2018-01-30', NULL, '0', '0', '0', 'null', 'docsanchezz@yahoo.com'),
(35, '2018-01-27', NULL, '4', '6', '2', 'null', 'docsanchezz@yahoo.com'),
(36, '2018-01-31', NULL, '0', '0', '0', 'null', 'docsanchezz@yahoo.com'),
(37, '2018-01-26', NULL, '5', '4', '7', 'null', 'docsanchezz@yahoo.com'),
(38, '2017-12-30', NULL, '0', '0', '0', 'null', 'ashe@betaar3.com'),
(39, '2017-12-28', NULL, '0', '0', '0', 'null', 'ashe@betaar3.com'),
(40, '2017-12-24', NULL, '10', '20', '30', 'null', 'ashe@betaar3.com'),
(41, '2017-12-25', NULL, '15', '25', '20', 'null', 'ashe@betaar3.com'),
(42, '2017-12-29', NULL, '0', '0', '0', 'null', 'ashe@betaar3.com'),
(43, '2017-12-27', NULL, '0', '0', '0', 'null', 'ashe@betaar3.com'),
(44, '2017-12-26', NULL, '0', '0', '0', 'null', 'ashe@betaar3.com'),
(45, '2018-01-03', NULL, '0', '0', '0', '130', 'ashe@betaar3.com'),
(46, '2017-12-32', NULL, '10', '10', '50', '130', 'ashe@betaar3.com'),
(47, '2018-01-02', NULL, '15', '20', '50', '130', 'ashe@betaar3.com'),
(48, '2017-12-31', NULL, '5', '5', '5', '130', 'ashe@betaar3.com'),
(49, '2018-01-05', NULL, '0', '0', '0', '130', 'ashe@betaar3.com'),
(50, '2018-01-04', NULL, '0', '0', '0', '130', 'ashe@betaar3.com'),
(51, '2018-01-06', NULL, '0', '0', '0', '130', 'ashe@betaar3.com'),
(52, '2018-01-02', NULL, '23', '23', '23', '130', 'aaa@email.com'),
(53, '2017-12-32', NULL, '23', '23', '23', '130', 'aaa@email.com'),
(55, '2018-01-03', NULL, '23', '23', '23', '130', 'aaa@email.com'),
(56, '2018-01-05', NULL, '23', '23', '23', '130', 'aaa@email.com'),
(57, '2018-01-04', NULL, '23', '23', '23', '130', 'aaa@email.com'),
(59, '2017-12-20', NULL, '0', '0', '0', '130', 'aaa@email.com'),
(60, '2017-12-21', NULL, '0', '0', '0', '130', 'aaa@email.com'),
(61, '2017-12-19', NULL, '0', '0', '0', '130', 'aaa@email.com'),
(62, '2017-12-22', NULL, '0', '0', '0', '130', 'aaa@email.com'),
(63, '2017-12-23', NULL, '0', '0', '0', '130', 'aaa@email.com'),
(65, '2017-12-25', NULL, '23', '32', '32', '130', 'aaa@email.com'),
(66, '2018-01-03', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(67, '2017-12-32', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(68, '2018-01-05', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(69, '2018-01-04', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(70, '2018-01-02', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(71, '2018-01-06', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(72, '2017-12-31', NULL, '10', '10', '10', '130', 'ashe11@betaar3.com'),
(73, '2017-12-29', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(74, '2017-12-30', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(75, '2018-01-13', NULL, '18', '10', '10', '130', 'ashe11@betaar3.com'),
(77, '2018-01-12', NULL, '10', '10', '10', '130', 'ashe11@betaar3.com'),
(78, '2018-01-09', NULL, '10', '20', '28', '130', 'ashe11@betaar3.com'),
(79, '2018-01-07', NULL, '10', '10', '10', '130', 'ashe11@betaar3.com'),
(80, '2018-01-10', NULL, '10', '20', '30', '130', 'ashe11@betaar3.com'),
(81, '2018-01-11', NULL, '10', '20', '30', '130', 'ashe11@betaar3.com'),
(82, '2018-01-20', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(83, '2018-01-23', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(84, '2018-01-21', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(85, '2018-01-24', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(86, '2018-01-22', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(87, '2018-01-26', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com'),
(88, '2018-01-25', NULL, '0', '0', '0', '130', 'ashe11@betaar3.com');

-- --------------------------------------------------------

--
-- Table structure for table `setsrepsgoal`
--

CREATE TABLE IF NOT EXISTS `setsrepsgoal` (
  `set_id` int(10) NOT NULL,
  `sets` int(3) NOT NULL,
  `reps` int(3) NOT NULL,
  `video_id` int(10) NOT NULL,
  `order_number` int(10) NOT NULL,
  `time_interval` int(5) NOT NULL,
  `date` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setsrepsgoal`
--

INSERT INTO `setsrepsgoal` (`set_id`, `sets`, `reps`, `video_id`, `order_number`, `time_interval`, `date`, `email`) VALUES
(0, 0, 0, 0, 3, 0, '2018-01-20', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-20', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-20', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-20', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-20', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-21', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-21', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-21', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-21', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 3, 0, '2018-01-21', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-22', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-22', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-22', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 3, 0, '2018-01-22', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-22', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-23', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-23', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-23', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-23', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 3, 0, '2018-01-23', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-24', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-24', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 3, 0, '2018-01-24', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-24', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-24', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 3, 0, '2018-01-25', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-25', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-25', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-25', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-25', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 1, 0, '2018-01-26', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 3, 0, '2018-01-26', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 2, 0, '2018-01-26', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 0, 0, '2018-01-26', 'ashe11@betaar3.com'),
(0, 0, 0, 0, 4, 0, '2018-01-26', 'ashe11@betaar3.com');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `category_id` int(1) NOT NULL,
  `subcategory_id` int(2) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`subcategory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`category_id`, `subcategory_id`, `category_name`) VALUES
(2, 1, 'Yoga'),
(2, 2, 'Dynamic Warm up'),
(2, 3, 'Pilaltes'),
(1, 4, 'Roller'),
(3, 5, 'Sport Training'),
(3, 6, 'Equipment Training');

-- --------------------------------------------------------

--
-- Table structure for table `testchart`
--

CREATE TABLE IF NOT EXISTS `testchart` (
  `userid` int(2) NOT NULL,
  `start_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testchart`
--

INSERT INTO `testchart` (`userid`, `start_date`) VALUES
(1, '2017-09-03'),
(2, '2017-09-04'),
(0, '2017-09-03'),
(0, '2017-09-03'),
(1, '2017-09-05'),
(2, '2017-09-05'),
(1, '2017-09-06'),
(2, '2017-09-07'),
(0, '2017-09-06'),
(0, '2017-09-06'),
(1, '2017-09-08'),
(2, '2017-09-08'),
(1, '2017-09-09'),
(0, '2017-09-09'),
(0, '2017-09-09'),
(1, '2017-09-13'),
(2, '2017-09-14'),
(0, '2017-09-15'),
(0, '2017-09-15'),
(1, '2017-09-16'),
(2, '2017-09-16'),
(1, '2017-09-17'),
(0, '2017-09-17'),
(0, '2017-09-17');

-- --------------------------------------------------------

--
-- Table structure for table `useractivity`
--

CREATE TABLE IF NOT EXISTS `useractivity` (
  `video_id` int(100) NOT NULL,
  `order_number` int(100) NOT NULL,
  `sets` int(10) NOT NULL,
  `reps` int(10) NOT NULL,
  `user_weight` varchar(6) NOT NULL default '0',
  `pain_level` int(10) NOT NULL default '0',
  `time_interval` varchar(5) NOT NULL,
  `email` varchar(50) NOT NULL,
  `curr_date` date NOT NULL,
  `curr_time` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useractivity`
--

INSERT INTO `useractivity` (`video_id`, `order_number`, `sets`, `reps`, `user_weight`, `pain_level`, `time_interval`, `email`, `curr_date`, `curr_time`) VALUES
(1, 0, 21, 21, '0', 0, '154', 'aaa@email.com', '2017-12-19', '05:50:22'),
(1, 2, 21, 21, '0', 0, '154', 'aaa@email.com', '2017-12-19', '05:50:22'),
(1, 1, 21, 21, '0', 0, '107', 'aaa@email.com', '2017-12-19', '05:50:21'),
(1, 0, 56, 23, '0', 0, '60', 'aaa@email.com', '2017-12-26', '05:12:36'),
(1, 2, 50, 23, '0', 0, '60', 'aaa@email.com', '2017-12-26', '05:12:36'),
(1, 1, 50, 23, '0', 0, '60', 'aaa@email.com', '2017-12-26', '05:12:36'),
(105, 0, 12, 2, '0', 0, '60', 'bbb@email.com', '2018-01-12', '00:04:38'),
(2, 1, 3232, 32, '0', 0, '60', 'aaa@email.com', '2017-12-26', '09:20:19'),
(2, 0, 32, 2332, '0', 0, '60', 'aaa@email.com', '2017-12-26', '09:20:20'),
(2, 2, 32, 32, '0', 0, '60', 'aaa@email.com', '2017-12-26', '09:20:20'),
(2, 3, 32, 32, '0', 0, '60', 'aaa@email.com', '2017-12-26', '09:20:20'),
(105, 2, 21, 12, '0', 0, '60', 'bbb@email.com', '2018-01-12', '00:04:39'),
(105, 1, 123, 123, '0', 0, '60', 'bbb@email.com', '2018-01-12', '00:04:38'),
(105, 3, 32, 21, '0', 0, '60', 'bbb@email.com', '2018-01-12', '00:04:40'),
(109, 0, 50, 5, '0', 0, '60', 'ashe@betaar3.com', '2018-01-17', '18:30:44'),
(109, 0, 50, 5, '0', 0, '60', 'ashe@betaar3.com', '2018-01-18', '01:44:32'),
(104, 0, 55, 5, '0', 0, '60', 'ashe@betaar3.com', '2018-01-18', '02:10:09'),
(104, 0, 68, 10, '0', 0, '60', '', '2018-01-18', '05:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `user_name` varchar(50) NOT NULL default 'none',
  `user_email` varchar(50) NOT NULL default 'none',
  `user_mobile` varchar(15) NOT NULL default 'none',
  `user_registration_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `user_address` varchar(200) NOT NULL default 'none',
  `user_age` varchar(8) NOT NULL default 'none',
  `user_weight` varchar(8) NOT NULL default 'none',
  `user_height` varchar(8) NOT NULL default 'none',
  `user_medical_detail` varchar(200) NOT NULL default 'none',
  `user_permission` varchar(10) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`user_name`, `user_email`, `user_mobile`, `user_registration_date`, `user_address`, `user_age`, `user_weight`, `user_height`, `user_medical_detail`, `user_permission`) VALUES
('null', 'aaa@email.com', 'null', '0000-00-00 00:00:00', '', '30', '150', '180', '', ''),
('sdfgasdf', '111aa@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdfgasdf', 'sdsdaq@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdfgasdf', 'aaaaaaaaaaaaaa@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdfgasdf', 'xcdes@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdfgasdf', 'qwawe123@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdfgasdf', '0986@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdfgasdf', 'z@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('qqqqqwwww', 'zz@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdserweflffgsf', 'zzaass@yahoo.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('sdserweflffgsf', 'zzaasswer@yahoo.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('12121212', 'z111z@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('12121212', 'z1111z@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'bbb', 'null'),
('xcvzxcvzxcv', 'zxcvzxcv@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', '123123', 'null'),
('bbbbb', 'bbbbb@email.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', '12342134', 'null'),
('ashe', 'wow@betaar3.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'no deal', 'null'),
('arvinda', '9911900738@gmail.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'test', 'null'),
('null', 'docsanchezz@yahoo.com', 'null', '0000-00-00 00:00:00', 'null', '47', '1', '57', '', 'null'),
('arvinda', '991190073@gmail.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'test', 'null'),
('ar', 'arvinda11@gmail.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'fff', 'null'),
('null', 'ashe11@betaar3.com', 'null', '0000-00-00 00:00:00', 'null', '', '', '', '', 'null'),
('ashe', 'ashe111@gmail.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'test', 'null'),
('ashe', 'ashe1111@gmail.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', '12', 'null'),
('ashe', 'ashe11@gmail.com', 'none', '0000-00-00 00:00:00', 'null', 'null', 'null', 'null', 'qqq', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(20) NOT NULL,
  `photo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--


-- --------------------------------------------------------

--
-- Table structure for table `uservideo`
--

CREATE TABLE IF NOT EXISTS `uservideo` (
  `video_id` int(11) NOT NULL auto_increment,
  `user_id` varchar(100) NOT NULL,
  `category_id` int(2) NOT NULL,
  `bodypart_id` varchar(10) NOT NULL,
  `subcategory_id` int(2) default NULL,
  `filename` varchar(100) NOT NULL,
  `image_url` varchar(200) NOT NULL,
  `video_url` varchar(200) NOT NULL,
  `video_title` varchar(50) NOT NULL,
  `file_type` varchar(10) NOT NULL,
  `date_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `delete_flag` varchar(1) NOT NULL default 'N',
  PRIMARY KEY  (`video_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `uservideo`
--

INSERT INTO `uservideo` (`video_id`, `user_id`, `category_id`, `bodypart_id`, `subcategory_id`, `filename`, `image_url`, `video_url`, `video_title`, `file_type`, `date_time`, `delete_flag`) VALUES
(1, 'aaa@email.com', 3, 'abs', NULL, 'cropped2111732880.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped2111732880.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped2111732880.jpg', 'null', 'image', '2017-11-10 00:52:25', 'N'),
(2, 'aaa@email.com', 3, 'quad', NULL, 'cropped-664829504.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-664829504.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-664829504.jpg', 'null', 'image', '2017-11-10 04:04:45', 'N'),
(3, 'aaa@email.com', 3, 'quad', NULL, 'cropped304619735.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped304619735.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped304619735.jpg', 'null', 'image', '2017-11-10 05:31:10', 'N'),
(4, 'aaa@email.com', 3, 'quad', NULL, 'cropped331577638.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped331577638.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped331577638.jpg', 'null', 'image', '2017-11-10 09:42:33', 'N'),
(5, 'aaa@email.com', 3, 'quad', NULL, 'cropped-1686877828.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1686877828.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1686877828.jpg', 'null', 'image', '2017-11-10 09:42:56', 'N'),
(6, 'null', 0, 'null', NULL, 'cropped-676921407.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-676921407.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-676921407.jpg', 'null', 'image', '2017-12-03 20:42:49', 'N'),
(7, 'aaa@email.com', 3, 'chest', NULL, 'cropped1990553278.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1990553278.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1990553278.jpg', 'null', 'image', '2017-12-06 18:46:45', 'N'),
(8, 'aaa@email.com', 3, 'chest', NULL, 'cropped-49874792.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-49874792.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-49874792.jpg', 'null', 'image', '2017-12-14 04:33:47', 'N'),
(9, 'docsanchezz@yahoo.com', 1, 'ankle', NULL, '1513462123648.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513462123648.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513462123648.mp4', 'null', 'video', '2017-12-16 14:09:29', 'N'),
(10, 'docsanchezz@yahoo.com', 3, 'calf', NULL, '1513462230676.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513462230676.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513462230676.mp4', 'null', 'video', '2017-12-16 14:10:27', 'N'),
(11, 'docsanchezz@yahoo.com', 1, 'mid back', NULL, 'cropped1738504438.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1738504438.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1738504438.jpg', 'null', 'image', '2017-12-16 15:03:37', 'N'),
(12, 'ashe@betaar3.com', 3, 'chest', NULL, 'cropped735667940.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped735667940.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped735667940.jpg', 'null', 'image', '2017-12-17 17:28:28', 'N'),
(13, 'ashe@betaar3.com', 1, 'wrist', NULL, 'cropped-1748995771.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1748995771.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1748995771.jpg', 'null', 'image', '2017-12-17 17:29:28', 'N'),
(14, 'ashe@betaar3.com', 1, 'quad', NULL, '1513560602270.mp4', 'http://173.8.145.131:8080/1fitlab/users/Thumbnail.jpg', 'http://173.8.145.131:8080/1fitlab/users/1513560602270.mp4', 'null', 'video', '2018-01-15 00:00:03', 'N'),
(15, 'ashe@betaar3.com', 3, 'chest', NULL, 'cropped1092417865.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1092417865.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1092417865.jpg', 'null', 'image', '2017-12-17 17:30:30', 'N'),
(16, 'docsanchezz@yahoo.com', 3, 'fore arm', NULL, 'cropped660269427.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped660269427.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped660269427.jpg', 'null', 'image', '2017-12-17 20:01:09', 'N'),
(17, 'docsanchezz@yahoo.com', 3, 'chest', NULL, 'cropped454465403.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped454465403.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped454465403.jpg', 'null', 'image', '2017-12-17 20:05:18', 'N'),
(18, 'aaa@email.com', 3, 'shoulder', NULL, '1513579732966.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513579732966.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513579732966.mp4', 'null', 'video', '2017-12-18 00:27:27', 'N'),
(19, 'aaa@email.com', 1, 'bicep', NULL, '1513579744626.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513579744626.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513579744626.mp4', 'null', 'video', '2017-12-18 00:27:44', 'N'),
(20, 'aaa@email.com', 1, 'chest', NULL, '1513612132016.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513612132016.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513612132016.mp4', 'null', 'video', '2017-12-18 07:49:44', 'N'),
(21, 'aaa@email.com', 3, 'chest', NULL, '1513619519776.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513619519776.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513619519776.mp4', 'null', 'video', '2017-12-18 09:52:12', 'N'),
(22, 'aaa@email.com', 3, 'shin', NULL, 'cropped1248963674.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1248963674.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1248963674.jpg', 'null', 'image', '2017-12-18 18:37:03', 'N'),
(23, 'aaa@email.com', 3, 'shin', NULL, '1513737924697.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513737924697.mp4', 'http://173.8.145.131:8080/1fitlab/users/1513737924697.mp4', 'null', 'video', '2017-12-19 18:48:42', 'N'),
(24, 'ashe11@betaar3.com', 1, 'forearm', NULL, 'cropped-1415344892.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1415344892.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1415344892.jpg', 'null', 'image', '2017-12-25 06:01:41', 'N'),
(25, 'null', 1, 'shoulder', NULL, 'cropped1506052232.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1506052232.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1506052232.jpg', 'null', 'image', '2017-12-25 08:09:00', 'N'),
(26, 'ashe@betaar3.com', 1, 'chest', NULL, 'cropped-1203018097.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1203018097.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1203018097.jpg', 'null', 'image', '2017-12-25 22:30:24', 'N'),
(27, 'ashe11@betaar3.com', 1, 'wrist', NULL, '1514278593559.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514278593559.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514278593559.mp4', 'null', 'video', '2017-12-26 00:57:31', 'N'),
(28, 'ashe@betaar3.com', 1, 'shoulder', NULL, 'cropped-248730312.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-248730312.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-248730312.jpg', 'null', 'image', '2017-12-27 23:54:12', 'N'),
(29, 'ashe@betaar3.com', 1, 'shoulder', NULL, 'cropped-1836114094.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1836114094.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1836114094.jpg', 'null', 'image', '2017-12-27 23:54:50', 'N'),
(30, 'ashe@betaar3.com', 1, 'shoulder', NULL, '1514447765684.mp4', 'http://173.8.145.131:8080/1fitlab/users/Thumbnail.jpg', 'http://173.8.145.131:8080/1fitlab/users/1514447765684.mp4', 'null', 'video', '2018-01-15 00:00:03', 'N'),
(31, 'ashe@betaar3.com', 1, 'shoulder', NULL, 'cropped-398537213.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-398537213.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-398537213.jpg', 'null', 'image', '2017-12-27 23:57:37', 'N'),
(32, 'ashe@betaar3.com', 1, 'shoulder', NULL, '1514447868675.mp4', 'http://173.8.145.131:8080/1fitlab/users/Thumbnail.jpg', 'http://173.8.145.131:8080/1fitlab/users/1514447868675.mp4', 'null', 'video', '2018-01-15 00:00:03', 'N'),
(33, 'docsanchezz@yahoo.com', 1, 'chest', NULL, 'cropped786366682.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped786366682.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped786366682.jpg', 'null', 'image', '2017-12-28 13:51:46', 'N'),
(34, 'ashe@betaar3.com', 1, 'forearm', NULL, 'cropped-1604547807.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1604547807.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1604547807.jpg', 'null', 'image', '2017-12-28 13:53:14', 'N'),
(35, 'ashe@betaar3.com', 3, 'chest', NULL, 'cropped-1515664131.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1515664131.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-1515664131.jpg', 'null', 'image', '2017-12-28 13:55:05', 'N'),
(36, 'docsanchezz@yahoo.com', 3, 'chest', NULL, 'cropped1088580806.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1088580806.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1088580806.jpg', 'null', 'image', '2017-12-29 17:10:56', 'N'),
(37, 'docsanchezz@yahoo.com', 1, 'quad', NULL, '1514596392340.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514596392340.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514596392340.mp4', 'null', 'video', '2017-12-29 17:15:45', 'N'),
(38, 'ashe@betaar3.com', 1, 'shoulder', NULL, 'cropped376552262.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped376552262.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped376552262.jpg', 'null', 'image', '2017-12-29 21:56:02', 'N'),
(39, 'docsanchezz@yahoo.com', 3, 'shoulder', NULL, '1514654948527.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514654948527.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514654948527.mp4', 'null', 'video', '2017-12-30 09:35:45', 'N'),
(40, 'ashe@betaar3.com', 3, 'shoulder', NULL, '1514683754428.mp4', 'http://173.8.145.131:8080/1fitlab/users/Thumbnail.jpg', 'http://173.8.145.131:8080/1fitlab/users/1514683754428.mp4', 'null', 'video', '2018-01-14 23:54:02', 'N'),
(41, 'ashe11@gmail.com', 3, 'shoulder', NULL, '1514880484644.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514880484644.mp4', 'http://173.8.145.131:8080/1fitlab/users/1514880484644.mp4', 'null', 'video', '2018-01-02 00:08:45', 'N'),
(42, 'ashe@betaar3.com', 1, 'shoulder', NULL, '1514949645012.mp4', 'http://173.8.145.131:8080/1fitlab/users/Thumbnail.jpg', 'http://173.8.145.131:8080/1fitlab/users/1514949645012.mp4', 'null', 'video', '2018-01-15 00:00:03', 'N'),
(43, 'docsanchezz@yahoo.com', 3, 'shoulder', NULL, 'cropped1196674548.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1196674548.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped1196674548.jpg', 'null', 'image', '2018-01-02 19:46:40', 'N'),
(44, 'null', 1, 'quad', NULL, '1515042915966.mp4', 'http://173.8.145.131:8080/1fitlab/users/1515042915966.mp4', 'http://173.8.145.131:8080/1fitlab/users/1515042915966.mp4', 'null', 'video', '2018-01-03 21:16:25', 'N'),
(45, 'ashe11@betaar3.com', 3, 'chest', NULL, 'cropped-902063503.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-902063503.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-902063503.jpg', 'null', 'image', '2018-01-09 02:03:09', 'N'),
(46, 'ashe11@betaar3.com', 3, 'bicep', NULL, 'cropped-2004226836.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-2004226836.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-2004226836.jpg', 'null', 'image', '2018-01-09 02:09:39', 'N'),
(47, 'ashe@betaar3.com', 3, 'shoulder', NULL, 'cropped-952558179.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-952558179.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-952558179.jpg', 'null', 'image', '2018-01-10 12:26:56', 'N'),
(48, 'ashe@betaar3.com', 3, 'shoulder', NULL, '1515712538458.mp4', 'http://173.8.145.131:8080/1fitlab/users/Thumbnail.jpg', 'http://173.8.145.131:8080/1fitlab/users/1515712538458.mp4', 'null', 'video', '2018-01-14 23:49:54', 'N'),
(49, 'ashe@betaar3.com', 1, 'bicep', NULL, 'cropped-176284916.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-176284916.jpg', 'http://173.8.145.131:8080/1fitlab/users/cropped-176284916.jpg', 'null', 'image', '2018-01-18 02:46:04', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `userweight`
--

CREATE TABLE IF NOT EXISTS `userweight` (
  `user_id` varchar(100) NOT NULL,
  `user_weight` varchar(3) NOT NULL,
  `datetime` varchar(21) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userweight`
--

INSERT INTO `userweight` (`user_id`, `user_weight`, `datetime`) VALUES
('arvinda@betaar3.com', '20', '2017-09-04'),
('arvinda@betaar3.com', '20', '2017-09-04'),
('arvinda@betaar3.com', '20', '2018-01-14'),
('ashe11@betaar3.com', '20', '2018-01-14'),
('aaa@email.com', '80', '2018-01-15'),
('ashe11@betaar3.com', '20', '2018-01-12'),
('aaa@email.com', '80', '2018-01-15'),
('aaa@email.com', '80', '2018-01-15'),
('', '80', '2018-01-16'),
('aaa@email.com', '80', '2018-01-16'),
('aaa@email.com', '80', '2018-01-16'),
('', '180', '2018-01-15'),
('aaa@email.com', '80', '2018-01-17'),
('ashe@betaar3.com', '210', '2018-01-16'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-17'),
('aaa@email.com', '80', '2018-01-18'),
('ashe@betaar3.com', '202', '2018-01-17'),
('ashe11@betaar3.com', '65', '2018-01-18'),
('ashe@betaar3.com', '202', '2018-01-18'),
('ashe@betaar3.com', '220', '2018-01-18'),
('aaa@email.com', '80', '2018-01-19'),
('aaa@email.com', '80', '2018-01-19'),
('ashe11@betaar3.com', '65', '2018-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `userworkout`
--

CREATE TABLE IF NOT EXISTS `userworkout` (
  `userid` varchar(100) NOT NULL,
  `workout_subcategory` int(3) NOT NULL,
  `bodypart_id` varchar(10) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` varchar(10) NOT NULL,
  `end_date` varchar(10) default NULL,
  `end_time` varchar(10) default NULL,
  `pain_level` int(2) NOT NULL,
  `user_weight` varchar(6) NOT NULL default '0',
  `timer` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userworkout`
--

INSERT INTO `userworkout` (`userid`, `workout_subcategory`, `bodypart_id`, `start_date`, `start_time`, `end_date`, `end_time`, `pain_level`, `user_weight`, `timer`) VALUES
('arvinda@betaar3.com', 1, '', '2017-09-04', '05:00:00', '2017-09-04', '05:15:20', 4, 'null', 10),
('ashe@betaar3.com', 4, '', '2017-09-04', '05:00:00', '2017-09-04', '05:15:20', 7, '0', 10),
('aa@betaar3.com', 5, '', '2017-09-04', '05:00:00', '2017-09-04', '05:15:20', 6, '0', 10),
('aaa@betaar3.com', 5, '', '2017-09-04', '05:00:00', '2017-09-04', '05:15:20', 6, '0', 10),
('bbbb@betaar3.com', 6, '', '2017-09-04', '05:00:00', '2017-09-04', '05:15:20', 6, '0', 10),
('bbbb@betaar3.com', 1, '', '2017-09-04', '05:00:00', '2017-09-04', '05:15:20', 6, '0', 10),
('aaa@email.com', 1, 'fv002', '2017-11-29', '06:31:36', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:32:06', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:33:51', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:34:22', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:36:01', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:36:20', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:36:47', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:37:00', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:37:14', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:37:23', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:37:36', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:37:48', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:38:26', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:38:37', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:38:48', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:38:56', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:39:05', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:39:11', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:39:28', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:39:41', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:40:11', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:40:32', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:41:40', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2017-11-16', '06:42:21', NULL, NULL, 2, '0', 9),
('aaa@email.com', 1, 'fv002', '2018-01-04', '06:19:26', NULL, NULL, 6, '0', 10),
('ashe11@betaar3.com', 1, 'fv002', '2018-01-04', '06:19:59', NULL, NULL, 6, '0', 10),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:01:07', NULL, NULL, 7, '0', 89),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:01:17', NULL, NULL, 7, '0', 89),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:02:02', NULL, NULL, 7, '0', 33),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:02:07', NULL, NULL, 7, '0', 33),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:03:23', NULL, NULL, 7, '0', 33),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:03:29', NULL, NULL, 7, '0', 33),
('ashe@betaar3.com', 119, 'fv001', '2018-01-05', '07:03:39', NULL, NULL, 7, '0', 33),
('ashe@betaar3.com', 1, 'fv002', '2018-01-05', '21:54:49', NULL, NULL, 6, '0', 10),
('ashe@betaar3.com', 1, 'fv002', '2018-01-04', '21:57:04', NULL, NULL, 6, '0', 10),
('ashe@betaar3.com', 1, 'fv002', '2018-01-04', '21:57:09', NULL, NULL, 6, '0', 10),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '01:05:26', NULL, NULL, 4, '220', 19),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '01:05:31', NULL, NULL, 4, '220', 19),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '01:06:07', NULL, NULL, 4, '220', 19),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '01:06:10', NULL, NULL, 4, '220', 19),
('', 123, 'fv006', '2018-01-07', '23:34:10', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:11', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:15', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:15', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:16', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:16', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:16', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:16', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:19', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:19', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:19', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:20', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:20', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:20', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:21', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:22', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:23', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:23', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:23', NULL, NULL, 6, '0', 60),
('', 123, 'fv006', '2018-01-07', '23:34:24', NULL, NULL, 6, '0', 60),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '23:34:28', NULL, NULL, 5, '220', 18),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '23:34:32', NULL, NULL, 5, '220', 18),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '23:34:59', NULL, NULL, 5, '220', 18),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '23:35:00', NULL, NULL, 5, '220', 18),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '23:45:37', NULL, NULL, 10, '220', 20),
('ashe@betaar3.com', 119, 'fv001', '2018-01-06', '23:47:22', NULL, NULL, 9, '220', 60),
('ashe@betaar3.com', 1, 'fv001', '2018-01-06', '23:59:53', NULL, NULL, 9, '220', 29),
('ashe@betaar3.com', 1, 'fv001', '2018-01-06', '00:00:04', NULL, NULL, 9, '220', 29),
('ashe11@betaar3.com', 1, 'fv002', '2018-01-07', '02:22:39', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 1, 'fv002', '2018-01-07', '02:22:55', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 1, 'fv002', '2018-01-07', '02:22:57', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 1, 'fv002', '2018-01-07', '02:23:00', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 1, 'fv005', '2018-01-07', '02:24:39', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 3, 'fv006', '2018-01-06', '02:34:00', NULL, NULL, 4, '0', 60),
('ashe11@betaar3.com', 2, 'fv008', '2018-01-07', '02:34:23', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 2, 'fv008', '2018-01-07', '02:34:45', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 2, 'fv008', '2018-01-06', '02:34:46', NULL, NULL, 1, '0', 60),
('ashe11@betaar3.com', 2, 'fv008', '2018-01-07', '02:34:46', NULL, NULL, 1, '0', 60),
('ashe@betaar3.com', 2, 'fv001', '2018-01-06', '11:27:36', NULL, NULL, 6, '0', 38),
('ashe@betaar3.com', 2, 'fv001', '2018-01-06', '11:28:41', NULL, NULL, 3, '0', 19),
('ashe@betaar3.com', 3, 'fv003', '2018-01-07', '18:48:44', NULL, NULL, 5, '0', 37),
('ashe@betaar3.com', 3, 'fv010', '2018-01-07', '18:50:33', NULL, NULL, 8, '0', 30),
('ashe@betaar3.com', 3, 'fv010', '2018-01-07', '18:51:11', NULL, NULL, 8, '0', 30),
('ashe@betaar3.com', 2, 'fv001', '2018-01-07', '21:52:06', NULL, NULL, 5, '0', 27),
('ashe11@betaar3.com', 1, 'fv002', '2018-01-09', '00:51:39', NULL, NULL, 4, '0', 5),
('ashe11@betaar3.com', 2, 'fv006', '2018-01-09', '00:56:49', NULL, NULL, 1, '0', 6),
('ashe11@betaar3.com', 2, 'fv006', '2018-01-09', '00:58:02', NULL, NULL, 1, '0', 6),
('ashe11@betaar3.com', 2, 'fv006', '2018-01-09', '00:58:52', NULL, NULL, 1, '0', 6),
('ashe11@betaar3.com', 2, 'fv006', '2018-01-09', '00:59:27', NULL, NULL, 1, '0', 6),
('ashe11@betaar3.com', 2, 'fv006', '2018-01-09', '01:16:57', NULL, NULL, 1, '0', 7),
('ashe11@betaar3.com', 2, 'fv006', '2018-01-09', '01:16:58', NULL, NULL, 1, '0', 7),
('ashe@betaar3.com', 2, 'fv001', '2018-01-10', '12:24:59', NULL, NULL, 1, '0', 60),
('ashe@betaar3.com', 2, 'fv001', '2018-01-11', '15:14:26', NULL, NULL, 6, '0', 60),
('ashe@betaar3.com', 2, 'fv001', '2018-01-11', '15:14:28', NULL, NULL, 6, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:08', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:10', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:12', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:14', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:23', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:23', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:24', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:24', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:24', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:24', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:25', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:25', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:25', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:26', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:26', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:27', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:27', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:27', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:27', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:28', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:28', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:28', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:28', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:29', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:29', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:29', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:29', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:30', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:30', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:57:33', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '23:58:03', NULL, NULL, 1, '0', 60),
('bbb@email.com', 1, 'fv006', '2018-01-12', '00:01:05', NULL, NULL, 1, '0', 60),
('ashe@betaar3.com', 1, 'fv001', '2018-01-15', '07:28:35', NULL, NULL, 7, '0', 30),
('ashe11@betaar3.com', 1, 'fv006', '2018-01-18', '22:53:40', NULL, NULL, 1, '0', 60),
('', 1, 'fv009', '2018-01-18', '02:10:44', NULL, NULL, 1, '0', 60),
('', 1, 'fv009', '2018-01-18', '02:10:48', NULL, NULL, 1, '0', 60),
('', 1, 'fv009', '2018-01-18', '02:11:05', NULL, NULL, 1, '0', 60),
('', 1, 'fv009', '2018-01-18', '02:11:13', NULL, NULL, 1, '0', 60),
('', 1, 'fv009', '2018-01-18', '02:11:16', NULL, NULL, 1, '0', 60),
('', 1, 'fv009', '2018-01-18', '02:11:18', NULL, NULL, 1, '0', 60);

-- --------------------------------------------------------

--
-- Table structure for table `workout`
--

CREATE TABLE IF NOT EXISTS `workout` (
  `workout_id` int(3) NOT NULL auto_increment,
  `workout_name` varchar(50) NOT NULL,
  `bodypart_id` varchar(5) NOT NULL,
  `workout_category_id` int(3) NOT NULL,
  `workout_subcategory` int(3) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `workout_time` varchar(10) NOT NULL,
  `workout_routine` varchar(20) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `workout_agegroup` varchar(10) NOT NULL,
  PRIMARY KEY  (`workout_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=160 ;

--
-- Dumping data for table `workout`
--

INSERT INTO `workout` (`workout_id`, `workout_name`, `bodypart_id`, `workout_category_id`, `workout_subcategory`, `gender`, `workout_time`, `workout_routine`, `comment`, `workout_agegroup`) VALUES
(1, 'Rolling', 'fv002', 1, 1, 'Male', '10', 'Daily', 'best  for shoulders', '30-40'),
(3, 'foag Roling', 'fv002', 1, 6, 'Male', '5', 'OnceWeek', 'done good', '20-40'),
(19, 'Test Exercise ', 'bv005', 1, 4, 'All', '10', 'Daily', 'this is test exercise. purpose to make shoulder strong and pain less.', '50-Onward'),
(18, 'Spanning Rolling ', 'bv004', 1, 4, 'All', '10', 'Daily', 'make back strong and flexible ', 'All'),
(11, 'foag', 'Neck', 1, 0, 'Male', '10', 'Daily', 'all', '20-40'),
(21, 'Roller-10', 'bv002', 1, 4, 'All', '10', 'Daily', 'for old men and woman. ', '50-Onward'),
(17, 'Rotating', 'bv009', 1, 4, 'All', '10', 'Daily', 'make wrist flexible ', 'All'),
(16, 'Kundalini Yoga', 'ABS', 1, 4, 'All', '10', 'Daily', 'concentration', 'All'),
(13, 'Yoga Bend', 'bv002', 2, 1, 'Male', '9', 'alternateDay', 'Legs make strong', '20-40'),
(14, 'Yoga Bend legs', 'Calf', 2, 1, '10', '10', 'OnceWeek', 'Done', '20-40'),
(20, 'test Roller 2', 'bv004', 1, 4, 'Male', '10', 'Daily', 'this is test roller. ', '20-40'),
(22, 'Roller test 32', 'sv003', 1, 4, 'Male', '10', 'Daily', 'test roller', '20-40'),
(23, 'Roller test 32', 'sv003', 1, 4, 'Male', '10', 'Daily', 'test roller', '20-40'),
(24, 'Roller-11', 'bv002', 1, 4, 'All', '10', 'Daily', 'Test roller 11', 'All'),
(25, 'test Roller 4', 'bv006', 1, 4, 'All', '10', 'Daily', 'test only', '50-Onward'),
(26, 'Roller-14', 'bv002', 1, 4, 'All', '10', 'Daily', 'test only', '20-40'),
(40, 'Neck', 'bv001', 2, 1, '10', '10', 'daily', 'Neck flexibility ', 'All'),
(41, 'Neck', 'bv001', 2, 1, '10', '10', 'daily', 'Neck flexibility ', 'All'),
(29, 'Yoga 3', 'bv001', 2, 1, '7', '7', 'alternateDay', 'do on alliterative day', 'All'),
(39, 'null', 'null', 2, 1, 'null', 'null', 'null', 'null', 'null'),
(38, 'null', 'null', 3, 6, 'null', 'null', 'null', 'null', 'null'),
(32, 'Warm Up name 02', 'bv002', 2, 2, '12', '12', 'daily', 'test warm', 'All'),
(33, 'Sport training test 2', 'bv003', 2, 2, '23', '23', 'daily', 'Soprt Training 1', '20-40'),
(34, 'Sport training test 3', 'bv001', 2, 2, '12', '12', 'daily', 'test Sport Training', '20-40'),
(35, 'test Roller 3', 'bv001', 2, 2, '12', '12', 'OnceWeek', '', '20-40'),
(36, 'Equipment Training', 'bv002', 3, 6, '23', '23', 'daily', '', '20-40'),
(37, 'Sport training test 6', 'sv002', 3, 2, '25', '25', 'daily', 'test sport training 01', '20-40'),
(42, 'Mid Back', 'bv002', 2, 1, '10', '10', 'daily', 'to protect pain', 'All'),
(43, 'Below Mid back', 'bv003', 2, 1, '10', '10', 'daily', 'protect below mid back pain', 'All'),
(44, 'lower back', 'bv004', 2, 1, '10', '10', 'daily', 'for Lower back pain', 'All'),
(45, 'Triceps', 'bv006', 2, 1, '10', '10', 'daily', 'for Triceps', 'All'),
(46, 'Elbow', 'bv007', 2, 1, '10', '10', 'daily', 'Elbow flexibility', 'All'),
(47, 'Fore Arm', 'bv008', 2, 1, '10', '10', 'daily', 'String Fore Arm', 'All'),
(48, 'Wrist', 'bv009', 2, 1, '10', '10', 'daily', 'Wrist', 'All'),
(49, 'Wrist', 'bv009', 2, 1, '10', '10', 'daily', 'Wrist', 'All'),
(50, 'Wrist', 'bv009', 2, 1, '10', '10', 'daily', 'Wrist', 'All'),
(51, 'Glute', 'bv010', 2, 1, '10', '10', 'daily', 'Glute Yoga', 'All'),
(52, 'Glute', 'bv010', 2, 1, '10', '10', 'daily', 'Glute Yoga', 'All'),
(53, 'Hamstring', 'bv011', 2, 1, '10', '10', 'daily', 'Hamstring', 'All'),
(54, 'Calf', 'bv012', 2, 1, '10', '10', 'daily', 'Calf', 'All'),
(55, 'Achilles', 'bv013', 2, 1, '10', '10', 'daily', 'Archilles', 'All'),
(56, 'Neck', 'sv001', 2, 1, '10', '10', 'daily', 'Neck', 'All'),
(57, 'Quad', 'sv008', 2, 1, '10', '10', 'daily', 'Quad Yoga', 'All'),
(58, 'ABS', 'fv006', 2, 1, '10', '10', 'daily', 'ABS Yoga', 'All'),
(59, 'Neck', 'bv001', 2, 2, '10', '10', 'Routine', 'for Neck flexibility', 'All'),
(60, 'Mid Back Sport', 'bv002', 2, 2, '10', '10', 'daily', 'Mid Back Sport', 'All'),
(61, 'Below Mid Back', 'bv003', 2, 2, '10', '10', 'daily', 'Below Mid Back Sport Training', 'All'),
(62, 'Lower Back Sport Training', 'bv004', 2, 2, '10', '10', 'daily', 'Lower Back Sport Training', 'All'),
(63, 'Triceps Sport Training', 'bv006', 2, 2, '10', '10', 'daily', 'Triceps Sport Training', 'All'),
(64, 'Elbow Sport Training', 'bv007', 2, 2, '10', '10', 'daily', 'Elbow Sport Training', 'All'),
(65, 'Fore Arm Sport Training', 'bv008', 2, 2, '10', '10', 'daily', 'Fore Arm Sport Training', 'All'),
(66, 'Wrist Sport Traning', 'bv009', 2, 2, '10', '10', 'daily', 'Wrist Sport Training', 'All'),
(67, 'Glute Sport Traning', 'bv010', 2, 2, '10', '10', 'daily', 'Glute Sport Traning', 'All'),
(68, 'Hamstring Sport Training', 'bv011', 2, 2, '10', '10', 'daily', 'Hamstring Sport Training', 'All'),
(69, 'Calf Sport Training', 'bv012', 2, 2, '10', '10', 'daily', 'Calf Sport Training', 'All'),
(70, 'Achilles Sport Training', 'bv013', 2, 2, '10', '10', 'daily', 'Achilles Sport Training', 'All'),
(71, 'Neck Sport Training', 'sv001', 2, 2, '10', '10', 'daily', 'Neck Sport Training', 'All'),
(72, 'Shoulders Sport Training', 'sv002', 2, 2, '10', '10', 'daily', 'Shoulders Sport Training', 'All'),
(73, 'Triceps Sport Training- Side', 'sv003', 2, 2, '10', '10', 'daily', 'Triceps Sport Training- Side', 'All'),
(74, 'Elbow Sport Training-Side', 'sv004', 2, 2, '10', '10', 'daily', 'Elbow Sport Training-Side', 'All'),
(75, 'Fore Arm Sport Training-Side', 'sv005', 2, 2, '10', '10', 'daily', 'Fore Arm Sport Training-Side', 'All'),
(76, 'Wrist Side Sport Training', 'sv006', 2, 2, '10', '10', 'daily', 'Wrist Side Sport Training', 'All'),
(77, 'Hamstring Sport Training-side', 'sv007', 2, 2, '10', '10', 'daily', 'Hamstring Sport Training-side', 'All'),
(78, 'Quad Sport Training-Side', 'sv008', 2, 2, '10', '10', 'daily', 'Quad Sport Training-Side', 'All'),
(79, 'Knee Sport Training-Side', 'sv009', 2, 2, '10', '10', 'daily', 'Knee Sport Training-Side', 'All'),
(80, 'Calf Sport Training-Side', 'sv010', 2, 2, '10', '10', 'daily', 'Calf Sport Training-Side', 'All'),
(81, 'Ankle Sport Training-Side', 'sv011', 2, 2, '10', '10', 'daily', 'Ankle Sport Training-Side', 'All'),
(82, 'Chest Sport Training-Front', 'fv001', 2, 2, '10', '10', 'daily', 'Chest Sport Training-Front', 'All'),
(83, 'Biceps Sport Training-Front', 'fv003', 2, 2, '10', '10', 'daily', 'Biceps Sport Training-Front', 'All'),
(84, 'ForeArm Sport Training-Front', 'fv004', 2, 2, '10', '10', 'daily', 'ForeArm Sport Training-Front', 'All'),
(85, 'Wrist Sport Training-Front', 'fv005', 2, 2, '10', '10', 'daily', 'Wrist Sport Training-Front', 'All'),
(86, 'ABS Sport Training-Front', 'fv006', 2, 2, '10', '10', 'daily', 'ABS Sport Training-Front', 'All'),
(87, 'Quad Sport Training', 'fv007', 2, 2, '10', '10', 'daily', 'Quad Sport Training', 'All'),
(88, 'Knee Sport Training-Front', 'fv008', 2, 2, '10', '10', 'daily', 'Knee Sport Training-Front', 'All'),
(89, 'Ankle Sport Training-Front', 'fv010', 2, 2, '10', '10', 'daily', 'Ankle Sport Training-Front', 'All'),
(90, 'Neck Yoga-Back', 'bv001', 2, 1, '10', '10', 'daily', 'Neck Yoga-Back', 'All'),
(91, 'Mid Back Yoga-Back', 'bv002', 2, 1, '10', '10', 'daily', 'Mid Back Yoga-Back', 'All'),
(92, 'Mid Back Yoga-Back', 'bv002', 2, 1, '10', '10', 'daily', 'Mid Back Yoga-Back', 'All'),
(93, 'Mid Back Yoga-Back', 'bv002', 2, 1, '10', '10', 'daily', 'Mid Back Yoga-Back', 'All'),
(94, 'Mid Back Yoga-Back', 'bv002', 2, 1, '10', '10', 'daily', 'Mid Back Yoga-Back', 'All'),
(95, 'Mid Back Yoga-Back', 'bv002', 2, 1, '10', '10', 'daily', 'Mid Back Yoga-Back', 'All'),
(96, 'Neck Roller-Back', 'bv001', 1, 4, 'All', '10', 'Daily', 'Neck Roller-Back', 'All'),
(97, 'Mid Back-Roller', 'bv002', 1, 4, 'All', '10', 'Daily', 'Mid Back-Roller', 'All'),
(98, 'Below Mid Back-Back', 'bv003', 1, 4, 'All', '10', 'Daily', 'Below Mid Back-Back', 'All'),
(99, 'Lower Back-back', 'bv004', 1, 4, 'All', '10', 'Daily', 'Lower Back-back', 'All'),
(100, 'Triceps Back-Roller', 'bv006', 1, 4, 'All', '10', 'Daily', 'Triceps Back-Roller', 'All'),
(101, 'Elbow Back-Roller', 'bv007', 1, 4, 'All', '10', 'Daily', 'Elbow Back-Roller', 'All'),
(102, 'Fore Arm Roller', 'bv008', 1, 4, 'All', '10', 'Daily', 'Fore Arm Roller', 'All'),
(103, 'Wrist Roller', 'bv009', 1, 4, 'All', '10', 'Daily', 'Wrist Roller', 'All'),
(104, 'Glute-Roller', 'bv010', 1, 4, 'All', '10', 'Daily', 'Glute-Roller', 'All'),
(105, 'Hamstring-Roller', 'bv011', 1, 4, 'All', '10', 'Daily', 'Hamstring-Roller', 'All'),
(106, 'Calf-Roller', 'bv012', 1, 4, 'All', '10', 'Daily', 'Calf-Roller', 'All'),
(107, 'Archiles-Roller', 'bv013', 1, 4, 'All', '10', 'Daily', 'Archiles-Roller', 'All'),
(108, 'Neck-Roller', 'sv001', 1, 4, 'All', '10', 'Daily', 'Neck-Roller', 'All'),
(109, 'Shoulders-Roller', 'sv002', 1, 4, 'All', '10', 'Daily', 'Shoulders-Roller', 'All'),
(110, 'Triceps-Roller', 'sv003', 1, 4, 'All', '10', 'Daily', 'Triceps-Roller', 'All'),
(111, 'Elbow-Roller', 'sv004', 1, 4, 'All', '10', 'Daily', 'Elbow-Roller', 'All'),
(112, 'Fore arm-Roller', 'sv005', 1, 4, 'All', '10', 'Daily', 'Fore arm-Roller', 'All'),
(113, 'Wrist-Roller', 'sv006', 1, 4, 'All', '10', 'Daily', 'Wrist-Roller', 'All'),
(114, 'Hamstring-Roller', 'sv007', 1, 4, 'All', '10', 'Daily', 'Hamstring-Roller', 'All'),
(115, 'Quad-Roller', 'sv008', 1, 4, 'All', '10', 'Daily', 'Quad-Roller', 'All'),
(116, 'Knee-Roller', 'sv009', 1, 4, 'All', '10', 'Daily', 'Knee-Roller', 'All'),
(117, 'Calf-Roller', 'sv010', 1, 4, 'All', '10', 'Daily', 'Calf-Roller', 'All'),
(118, 'Ankle-Roller', 'sv011', 1, 4, 'All', '10', 'Daily', 'Ankle-Roller', 'All'),
(119, 'Chest-Roller', 'fv001', 1, 4, 'All', '10', 'Daily', 'Chest-Roller', 'All'),
(120, 'Biceps-Roller', 'fv003', 1, 4, 'All', '10', 'Daily', 'Biceps-Roller', 'All'),
(121, 'Fore Arm-Roller', 'fv004', 1, 4, 'All', '10', 'Daily', 'Fore Arm-Roller', 'All'),
(122, 'Wrist-Roller', 'fv005', 1, 4, 'All', '10', 'Daily', 'Wrist-Roller', 'All'),
(123, 'ABS-Roller', 'fv006', 1, 4, 'All', '10', 'Daily', 'ABS-Roller', 'All'),
(124, 'Quad-Roller', 'fv007', 1, 4, 'All', '10', 'Daily', 'Quad-Roller', 'All'),
(125, 'Knee-Roller', 'fv008', 1, 4, 'All', '10', 'Daily', 'Knee-Roller', 'All'),
(126, 'Shin-Roller', 'fv009', 1, 4, 'All', '10', 'Daily', 'Shin-Roller', 'All'),
(127, 'Ankle-Roller', 'fv010', 1, 4, 'All', '10', 'Daily', 'Ankle-Roller', 'All'),
(128, 'Neck-Yoga', 'bv001', 2, 1, '10', '10', 'daily', 'Neck-Yoga', 'All'),
(129, 'Mid Back_yoga', 'bv002', 2, 1, '10', '10', 'daily', 'Mid Back_yoga', 'All'),
(130, 'Below Mid Back-Yoga', 'bv003', 2, 1, '10', '10', 'daily', 'Below Mid Back-Yoga', 'All'),
(131, 'Lower Back-Yoga', 'bv004', 2, 1, '10', '10', 'daily', 'Lower Back-Yoga', 'All'),
(132, 'Triceps-Yoga', 'bv006', 2, 1, '10', '10', 'daily', 'Triceps-Yoga', 'All'),
(133, 'Elbow-Yoga', 'bv007', 2, 1, '10', '10', 'daily', 'Elbow-Yoga', 'All'),
(134, 'Fore Arm-Yoga', 'bv008', 2, 1, '10', '10', 'daily', 'Fore Arm-Yoga', 'All'),
(135, 'Wrist-Yoga', 'bv009', 3, 1, '10', '10', 'daily', 'Wrist-Yoga', 'All'),
(136, 'Glute-Yoga', 'bv010', 3, 1, '10', '10', 'daily', 'Glute-Yoga', 'All'),
(137, 'Hamstring-Yoga', 'bv011', 3, 1, '10', '10', 'daily', 'Hamstring-Yoga', 'All'),
(138, 'Calf-Yoga', 'bv012', 3, 1, '10', '10', 'daily', 'Calf-Yoga', 'All'),
(139, 'Achiles-Yoga', 'bv013', 3, 1, '10', '10', 'daily', 'Achiles-Yoga', 'All'),
(140, 'Neck-Yoga', 'sv001', 3, 1, '10', '10', 'daily', 'Neck-Yoga', 'All'),
(141, 'Shoulders-Yoga', 'sv002', 3, 1, '10', '10', 'daily', 'Shoulders-Yoga', 'All'),
(142, 'Triceps-Yoga', 'sv003', 3, 1, '10', '10', 'daily', 'Triceps-Yoga', 'All'),
(143, 'Elbow-Yoga', 'sv004', 3, 1, '10', '10', 'daily', 'Elbow-Yoga', 'All'),
(144, 'Fore Arm-Yoga', 'sv005', 3, 1, '10', '10', 'daily', 'Fore Arm-Yoga', 'All'),
(145, 'Wrist_yoga', 'sv006', 3, 1, '11', '11', 'daily', 'Wrist_yoga', 'All'),
(146, 'Hamstring-Yoga', 'sv007', 3, 1, '10', '10', 'daily', 'Hamstring-Yoga', 'All'),
(147, 'Quad-Yoga', 'sv008', 3, 1, '10', '10', 'daily', 'Quad-Yoga', 'All'),
(148, 'Knee-Yoga', 'sv009', 3, 1, '10', '10', 'daily', 'Knee-Yoga', 'All'),
(149, 'Calf-Yoga', 'sv010', 3, 1, '10', '10', 'daily', 'Calf-Yoga', 'All'),
(150, 'Ankle-Yoga', 'sv011', 2, 1, '10', '10', 'daily', 'Ankle-Yoga', 'All'),
(151, 'Chest-Yoga', 'fv001', 3, 1, '10', '10', 'daily', 'Chest-Yoga', 'All'),
(152, 'Bicep-Yoga', 'fv003', 3, 1, '10', '10', 'daily', 'Bicep-Yoga', 'All'),
(153, 'Fore Arm-Yoga', 'fv004', 3, 1, '10', '10', 'daily', 'Fore Arm-Yoga', 'All'),
(154, 'Wrist-Yoga', 'fv005', 3, 1, '10', '10', 'daily', 'Wrist-Yoga', 'All'),
(155, 'ABS-Yoga', 'fv006', 3, 1, '10', '10', 'daily', 'ABS-Yoga', 'All'),
(156, 'Quad-Yoga', 'fv007', 3, 1, '10', '10', 'daily', 'Quad-Yoga', 'All'),
(157, 'Knee-Yoga', 'fv008', 3, 1, '10', '10', 'daily', 'Knee-Yoga', 'All'),
(158, 'Shin-Yoga', 'fv009', 3, 1, '10', '10', 'daily', 'Shin-Yoga', 'All'),
(159, 'Ankle-Yoga', 'fv010', 3, 1, '10', '10', 'daily', 'Ankle-Yoga', 'All');

-- --------------------------------------------------------

--
-- Table structure for table `workoutvideo`
--

CREATE TABLE IF NOT EXISTS `workoutvideo` (
  `video_id` int(100) NOT NULL auto_increment,
  `workout_id` int(5) NOT NULL,
  `workout_video_url` varchar(500) NOT NULL,
  `workout_image_url` varchar(500) NOT NULL,
  `video_title` varchar(100) NOT NULL,
  `video_description` varchar(100) NOT NULL,
  `datetime` datetime NOT NULL,
  `deleted_flag` int(1) NOT NULL default '1',
  PRIMARY KEY  (`video_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=210 ;

--
-- Dumping data for table `workoutvideo`
--

INSERT INTO `workoutvideo` (`video_id`, `workout_id`, `workout_video_url`, `workout_image_url`, `video_title`, `video_description`, `datetime`, `deleted_flag`) VALUES
(1, 1, 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind.jpg', 'Arvinda', 'test Video', '0000-00-00 00:00:00', 1),
(2, 3, 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Ashe', 'AVS PO for 107 Printed RFID cards Pg1.jpg', '2017-09-10 00:00:00', 1),
(88, 37, 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'sets and reps', 'to make strong bishape', '0000-00-00 00:00:00', 1),
(89, 58, 'http://173.8.145.131:8080/1fitlab/videos/Shoulders Back Chest and Arm Workout for Strong Toned Upper Body.mp3', 'http://173.8.145.131:8080/1fitlab/videos/Shoulders Back Chest and Arm Workout for Strong Toned Upper Body.mp3', 'ABS Stetching', 'Shoulders Back Chest and Arm Workout for Strong Toned Upper Body.mp3', '2017-09-10 00:00:00', 1),
(90, 57, 'http://173.8.145.131:8080/1fitlab/videos/S1.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S1.mp3', '', 'S1.mp3', '2017-09-10 00:00:00', 1),
(91, 56, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S2.mp3', '', 'S2.mp3', '2017-09-10 00:00:00', 1),
(92, 55, 'http://173.8.145.131:8080/1fitlab/videos/S3.mp3', 'http://173.8.145.131:8080/1fitlab/videos/S3.mp3', 'Stretching', 'S3.mp3', '2017-09-10 00:00:00', 1),
(93, 54, 'http://173.8.145.131:8080/1fitlab/videos/S3.mp3', 'http://173.8.145.131:8080/1fitlab/videos/S3.mp3', 'Stretching', 'S3.mp3', '2017-09-10 00:00:00', 1),
(94, 53, 'http://173.8.145.131:8080/1fitlab/videos/S4.mp4', 'http://173.8.145.131:8080/1fitlab/videos/S4.mp3', 'Stretching', 'S4.mp3', '2017-09-10 00:00:00', 1),
(95, 51, 'http://173.8.145.131:8080/1fitlab/videos/S5.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S5.mp3', 'Stretching', 'S5.mp3', '2017-09-10 00:00:00', 1),
(96, 49, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(97, 47, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(98, 46, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(99, 45, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(100, 44, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(101, 43, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(102, 42, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/S12.mp3', 'Stretching', 'S12.mp3', '2017-09-10 00:00:00', 1),
(103, 89, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(104, 88, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(105, 86, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(106, 85, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(107, 84, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(108, 83, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(109, 82, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(110, 81, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(111, 80, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(112, 79, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(113, 78, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(114, 77, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(115, 76, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(116, 75, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(117, 74, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(118, 73, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(119, 72, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(120, 71, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(121, 70, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(122, 69, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(123, 68, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(124, 67, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(125, 66, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(126, 65, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(127, 64, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(128, 63, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(129, 62, 'http://173.8.145.131:8080/1fitlab/videos/S12.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Sport Training', 'S12.mp3', '2017-09-10 00:00:00', 1),
(130, 90, 'http://173.8.145.131:8080/1fitlab/videos/S4.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S4.mp3', '2017-09-10 00:00:00', 1),
(131, 95, 'http://173.8.145.131:8080/1fitlab/videos/S4.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S4.mp3', '2017-09-10 00:00:00', 1),
(132, 95, 'http://173.8.145.131:8080/1fitlab/videos/S4.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S4.mp3', '2017-09-10 00:00:00', 1),
(133, 90, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(134, 91, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(135, 96, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(136, 98, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(137, 99, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(138, 100, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(139, 101, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(140, 102, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(141, 103, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(142, 104, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(143, 105, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(144, 106, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(145, 107, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(146, 108, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(147, 109, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(148, 110, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(149, 111, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(150, 112, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(151, 113, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(152, 114, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(153, 115, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(154, 116, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(155, 117, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(156, 118, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(157, 119, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(158, 120, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(159, 121, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(160, 122, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(161, 123, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(162, 124, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(163, 125, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(164, 126, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(165, 127, 'http://173.8.145.131:8080/1fitlab/videos/S6.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S6.mp3', '2017-09-10 00:00:00', 1),
(166, 128, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(167, 129, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(168, 130, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(169, 131, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(170, 132, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(171, 133, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(172, 134, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(173, 135, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(174, 136, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(175, 137, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(176, 138, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(177, 139, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(178, 140, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(179, 141, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(180, 142, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(181, 143, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(182, 144, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(183, 145, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(184, 146, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(185, 147, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(186, 148, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(187, 149, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(188, 150, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(189, 151, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(190, 152, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(191, 153, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(192, 154, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(193, 155, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(194, 156, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(195, 157, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(196, 158, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(197, 159, 'http://173.8.145.131:8080/1fitlab/videos/S2.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Yoga', 'S2.mp3', '2017-09-10 00:00:00', 1),
(198, 128, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(199, 129, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(200, 130, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(201, 131, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(202, 132, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(203, 133, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(204, 134, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(205, 136, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(206, 137, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(207, 138, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(208, 139, 'http://173.8.145.131:8080/1fitlab/videos/20161103_194621.mp4', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'Stretching', '20161103_194621.mp4', '2017-09-10 00:00:00', 1),
(209, 159, 'http://173.8.145.131:8080/1fitlab/videos/S1.3gp', 'http://173.8.145.131:8080/1fitlab/videos/leaf_blown_by_wind_1.jpg', 'video', 'S1.3gp', '2017-09-10 00:00:00', 1);
