===Download 
https://redis.io/download (windows & linux)

http://mirror.linux-ia64.org/apache/tomcat/tomcat-9/v9.0.21/bin/apache-tomcat-9.0.21.zip
http://mirror.linux-ia64.org/apache/tomcat/tomcat-9/v9.0.21/bin/apache-tomcat-9.0.21.exe
http://mirror.linux-ia64.org/apache/tomcat/tomcat-9/v9.0.21/bin/apache-tomcat-9.0.21-windows-x64.zip
http://mirror.linux-ia64.org/apache/tomcat/tomcat-8/v8.5.42/bin/apache-tomcat-8.5.42.zip
http://mirror.linux-ia64.org/apache/tomcat/tomcat-8/v8.5.42/bin/apache-tomcat-8.5.42.exe
http://mirror.linux-ia64.org/apache/tomcat/tomcat-8/v8.5.42/bin/apache-tomcat-8.5.42-windows-x64.zip

https://bitnami.com/redirect/to/587759/bitnami-tomcatstack-8.5.42-1-linux-x64-installer.run
https://bitnami.com/redirect/to/583895/bitnami-tomcatstack-9.0.21-1-dev-linux-x64-installer.run
https://bitnami.com/redirect/to/546079/bitnami-redis-5.0.5-0-linux-x64-installer.run

https://github.com/kohsuke/winsw/releases
https://github.com/wvengen/proguard-maven-plugin/releases
https://github.com/seregaSLM/proguard-spring-boot-example

intellij plugins(lombok, proguard)

===maven dependency

<!-- https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client -->
<dependency>
    <groupId>org.mariadb.jdbc</groupId>
    <artifactId>mariadb-java-client</artifactId>
    <version>2.4.2</version>
</dependency>


<!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-ehcache -->
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-ehcache</artifactId>
    <version>5.3.10.Final</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-jcache -->
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-jcache</artifactId>
    <version>5.3.10.Final</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.thymeleaf.extras/thymeleaf-extras-springsecurity5 -->
<dependency>
    <groupId>org.thymeleaf.extras</groupId>
    <artifactId>thymeleaf-extras-springsecurity5</artifactId>
    <version>3.0.4.RELEASE</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-messaging -->
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-messaging</artifactId>
    <version>5.0.9.RELEASE</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.springframework.security/spring-security-messaging -->
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-messaging</artifactId>
    <version>5.1.5.RELEASE</version>
</dependency>
<!-- https://mvnrepository.com/artifact/io.lettuce/lettuce-core -->
<dependency>
    <groupId>io.lettuce</groupId>
    <artifactId>lettuce-core</artifactId>
    <version>5.1.7.RELEASE</version>
</dependency>

<!-- https://mvnrepository.com/artifact/io.netty/netty-all -->
<dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.36.Final</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.apache.poi/poi -->
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi</artifactId>
    <version>4.1.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-assembly-plugin -->
<dependency>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-assembly-plugin</artifactId>
    <version>3.1.1</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-resources-plugin -->
<dependency>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-resources-plugin</artifactId>
    <version>2.6</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-compiler-plugin -->
<dependency>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.1</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-jar-plugin -->
<dependency>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <version>2.4</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.github.wvengen/proguard-maven-plugin -->
<dependency>
    <groupId>com.github.wvengen</groupId>
    <artifactId>proguard-maven-plugin</artifactId>
    <version>2.1.0</version>
</dependency>


